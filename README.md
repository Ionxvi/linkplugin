Test Input JSON: 


```
{
    "linkTable": [
        {
            "E1": "GOGL|http://google.com/1",
            "E2": "GOGL|http://google.com/2",
            "E3": "GOGL|http://google.com/3",
            "E4": "GOGL|http://google.com/4",
            "E5": "GOGL|http://google.com/5"
        },
        {
            "E1": "FBOOK|http://facebook.com/1",
            "E2": "FBOOK|http://facebook.com/2",
            "E3": "FBOOK|http://facebook.com/3",
            "E4": "FBOOK|http://facebook.com/4",
            "E5": "FBOOK|http://facebook.com/5"
        },
        {
            "E1": "TWTR|http://twitter.com/1",
            "E2": "TWTR|http://twitter.com/2",
            "E3": "TWTR|http://twitter.com/3",
            "E4": "TWTR|http://twitter.com/4",
            "E5": "TWTR|http://twitter.com/5"
        },
        {
            "E1": "INSTA|http://instagram.com/1",
            "E2": "INSTA|http://instagram.com/2",
            "E3": "INSTA|http://instagram.com/3",
            "E4": "INSTA|http://instagram.com/4",
            "E5": "INSTA|http://instagram.com/5"
        },
        {
            "E1": "SOUNCL|http://soundcloud.com/1",
            "E2": "SOUNCL|http://soundcloud.com/1",
            "E3": "SOUNCL|http://soundcloud.com/1",
            "E4": "SOUNCL|http://soundcloud.com/1",
            "E5": "SOUNCL|http://soundcloud.com/1"
        },
        {
            "E1": "SOUNCL|http://soundcloud.com/1",
            "E2": "SOUNCL|http://soundcloud.com/1",
            "E3": "SOUNCL|http://soundcloud.com/1",
            "E4": "SOUNCL|http://soundcloud.com/1",
            "E5": "SOUNCL|http://soundcloud.com/1"
        }
    ]
}
```
